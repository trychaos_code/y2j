const path = require('path');
const {YamlReader} = require('../index');

// No dependency test cases for given yml
const testFile = (filePath) => {
    const throwErr = (...err) => {
        console.error.apply(null,['✕ | ', ...err]);
    };
    const file = path.resolve(__dirname, filePath);
    try {
        const config = YamlReader.read(file);
        if(!config) {
            throwErr('Not able to parse => ', file);
        } else {
            console.log('✔ | Successfully parsed => ', file)
        }
    } catch (e) {
        throwErr(e.message, ' => ', file);
    }
};
testFile('./sample.yml');

module.exports = {testFile};
