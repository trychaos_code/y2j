
/**
    Class Pattern is a zero-conflict wrapper extending RegExp features
    in order to make YML parsing regex more expressive.
*/
class Reg {
    /**
     Constructor
     * @param rawRegex
     * @param modifiers
     */
    constructor(rawRegex, modifiers = '') {
        let _char, capturingBracketNumber, cleanedRegex, i, len, mapping, name, part, subChar;
        cleanedRegex = '';
        len = rawRegex.length;
        mapping = null;
        // Cleanup raw regex and compute mapping
        capturingBracketNumber = 0;
        i = 0;
        while (i < len) {
            _char = rawRegex.charAt(i);
            if (_char === '\\') {
                // Ignore next character
                cleanedRegex += rawRegex.slice(i, +(i + 1) + 1 || 9e9);
                i++;
            } else if (_char === '(') {
                // Increase bracket number, only if it is capturing
                if (i < len - 2) {
                    part = rawRegex.slice(i, +(i + 2) + 1 || 9e9);
                    if (part === '(?:') {
                        // Non-capturing bracket
                        i += 2;
                        cleanedRegex += part;
                    } else if (part === '(?<') {
                        // Capturing bracket with possibly a name
                        capturingBracketNumber++;
                        i += 2;
                        name = '';
                        while (i + 1 < len) {
                            subChar = rawRegex.charAt(i + 1);
                            if (subChar === '>') {
                                cleanedRegex += '(';
                                i++;
                                if (name.length > 0) {
                                    // Associate a name with a capturing bracket number
                                    if (mapping == null) {
                                        mapping = {};
                                    }
                                    mapping[name] = capturingBracketNumber;
                                }
                                break;
                            } else {
                                name += subChar;
                            }
                            i++;
                        }
                    } else {
                        cleanedRegex += _char;
                        capturingBracketNumber++;
                    }
                } else {
                    cleanedRegex += _char;
                }
            } else {
                cleanedRegex += _char;
            }
            i++;
        }
        this.cleanedRegex = cleanedRegex;
        this.regex = new RegExp(this.cleanedRegex, 'g' + modifiers.replace('g', ''));
        this.mapping = mapping;
    }

    /**
     Executes the pattern's regex and returns the matching values
     @return [Array] The matching values extracted from capturing brackets or null if nothing matched
     * @param str
     */
    exec(str) {
        let index, matches, name, ref;
        this.regex.lastIndex = 0;
        matches = this.regex.exec(str);
        if (matches == null) {
            return null;
        }
        if (this.mapping != null) {
            ref = this.mapping;
            for (name in ref) {
                index = ref[name];
                matches[name] = matches[index];
            }
        }
        return matches;
    }

    /**
     Tests the pattern's regex
     @return [Boolean] true if the string matched
     * @param str
     */
    test(str) {
        this.regex.lastIndex = 0;
        return this.regex.test(str);
    }

    /**
     Replaces occurences matching with the pattern's regex with replacement
     @return [String] The replaced string
     * @param str
     * @param replacement
     */
    replace(str, replacement) {
        this.regex.lastIndex = 0;
        return str.replace(this.regex, replacement);
    }

    /**
     Replaces occurences matching with the pattern's regex with replacement and
     get both the replaced string and the number of replaced occurences in the string.
     @return [Array] A destructurable array containing the replaced string and the number of replaced occurences. For instance: ["my replaced string", 2]
     * @param str
     * @param replacement
     * @param limit
     */

    replaceAll(str, replacement, limit = 0) {
        let count;
        this.regex.lastIndex = 0;
        count = 0;
        while (this.regex.test(str) && (limit === 0 || count < limit)) {
            this.regex.lastIndex = 0;
            str = str.replace(this.regex, replacement);
            count++;
        }
        return [str, count];
    }

}

module.exports = {Reg};
