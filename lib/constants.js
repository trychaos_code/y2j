const {Reg} = require('./custom-regex/Reg');
const fromCharCode = String.fromCharCode;
const REGEX_QUOTED_STRING = '(?:"(?:[^"\\\\]*(?:\\\\.[^"\\\\]*)*)"|\'(?:[^\']*(?:\'\'[^\']*)*)\')';
const escape = {
    // Mapping arrays for escaping a double quoted string. The backslash is
    LIST_ESCAPEES: ['\\', '\\\\', '\\"', '"', '\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07', '\x08', '\x09', '\x0a', '\x0b', '\x0c', '\x0d', '\x0e', '\x0f', '\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17', '\x18', '\x19', '\x1a', '\x1b', '\x1c', '\x1d', '\x1e', '\x1f', fromCharCode(0x0085), fromCharCode(0x00A0), fromCharCode(0x2028), fromCharCode(0x2029)],
    LIST_ESCAPED: ['\\\\', '\\"', '\\"', '\\"', '\\0', '\\x01', '\\x02', '\\x03', '\\x04', '\\x05', '\\x06', '\\a', '\\b', '\\t', '\\n', '\\v', '\\f', '\\r', '\\x0e', '\\x0f', '\\x10', '\\x11', '\\x12', '\\x13', '\\x14', '\\x15', '\\x16', '\\x17', '\\x18', '\\x19', '\\x1a', '\\e', '\\x1c', '\\x1d', '\\x1e', '\\x1f', '\\N', '\\_', '\\L', '\\P']
};
module.exports = {
    fromCharCode,
    REG: {
        REG_TRAILING_COMMENTS: new Reg('^\\s*#.*$'),
        REG_QUOTED_SCALAR: new Reg('^' + REGEX_QUOTED_STRING),
        REG_THOUSAND_NUMERIC_SCALAR: new Reg('^(-|\\+)?[0-9,]+(\\.[0-9]+)?$'),
        REG_FOLDED_SCALAR_ALL: new Reg('^(?:(?<type>![^\\|>]*)\\s+)?(?<separator>\\||>)(?<modifiers>\\+|\\-|\\d+|\\+\\d+|\\-\\d+|\\d+\\+|\\d+\\-)?(?<comments> +#.*)?$'),
        REG_FOLDED_SCALAR_END: new Reg('(?<separator>\\||>)(?<modifiers>\\+|\\-|\\d+|\\+\\d+|\\-\\d+|\\d+\\+|\\d+\\-)?(?<comments> +#.*)?$'),
        REG_SEQUENCE_ITEM: new Reg('^\\-((?<leadspaces>\\s+)(?<value>.+?))?\\s*$'),
        REG_ANCHOR_VALUE: new Reg('^&(?<ref>[^ ]+) *(?<value>.*)'),
        REG_COMPACT_NOTATION: new Reg('^(?<key>' + REGEX_QUOTED_STRING + '|[^ \'"\\{\\[].*?) *\\:(\\s+(?<value>.+?))?\\s*$'),
        REG_MAPPING_ITEM: new Reg('^(?<key>' + REGEX_QUOTED_STRING + '|[^ \'"\\[\\{].*?) *\\:(\\s+(?<value>.+?))?\\s*$'),
        REG_DECIMAL: new Reg('\\d+'),
        REG_YML_HEADER: new Reg('^\\%YAML[: ][\\d\\.]+.*\n', 'm'),
        REG_LEADING_COMMENTS: new Reg('^(\\#.*?\n)+', 'm'),
        REG_DOCUMENT_MARKER_START: new Reg('^\\-\\-\\-.*?\n', 'm'),
        REG_DOCUMENT_MARKER_END: new Reg('^\\.\\.\\.\\s*$', 'm'),
        // Characters that would cause a dumped string to require double quoting.
        REG_CHARACTERS_TO_ESCAPE: new Reg('[\\x00-\\x1f]|\xc2\x85|\xc2\xa0|\xe2\x80\xa8|\xe2\x80\xa9'),
        REG_MAPPING_ESCAPEES: new Reg(escape.LIST_ESCAPEES.join('|').split('\\').join('\\\\')),
        REG_SINGLE_QUOTING: new Reg('[\\s\'":{}[\\],&*#?]|^[-?|<>=!%@`]'),
        REG_ESCAPED_CHARACTER: new Reg('\\\\([0abt\tnvfre "\\/\\\\N_LP]|x[0-9a-fA-F]{2}|u[0-9a-fA-F]{4}|U[0-9a-fA-F]{8})'),
        REG_DATE: new Reg('^' + '(?<year>[0-9][0-9][0-9][0-9])' + '-(?<month>[0-9][0-9]?)' + '-(?<day>[0-9][0-9]?)' + '(?:(?:[Tt]|[ \t]+)' + '(?<hour>[0-9][0-9]?)' + ':(?<minute>[0-9][0-9])' + ':(?<second>[0-9][0-9])' + '(?:\.(?<fraction>[0-9]*))?' + '(?:[ \t]*(?<tz>Z|(?<tz_sign>[-+])(?<tz_hour>[0-9][0-9]?)' + '(?::(?<tz_minute>[0-9][0-9]))?))?)?' + '$', 'i'),
    },
    reg: {
        REGEX_SPACES: /\s+/g,
        REGEX_DIGITS: /^\d+$/,
        REGEX_OCTAL: /[^0-7]/gi,
    },
    static_field: {
        REG_SCALAR_BY_DELIMITERS: {},
        settings: {},
        REGEX_LEFT_TRIM_BY_CHAR: {},
        REGEX_RIGHT_TRIM_BY_CHAR: {},
    },
    escape
};
