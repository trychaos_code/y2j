
/**
    Class overridden from error class
*/
class ParseError extends Error {
    constructor(message, parsedLine, snippet) {
        super(message);
        this.message = message;
        this.parsedLine = parsedLine;
        this.snippet = snippet;
    }

    toString() {
        if ((this.parsedLine != null) && (this.snippet != null)) {
            return '<ParseError> ' + this.message + ' (line ' + this.parsedLine + ': \'' + this.snippet + '\')';
        } else {
            return '<ParseError> ' + this.message;
        }
    }

}

module.exports = {ParseError};
