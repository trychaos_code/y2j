const {REG, fromCharCode} = require('../constants');
const {Utils} = require('../utils');

/*
    Class AddEscape encapsulates escaping rules for single
    and double-quoted YML strings.
*/
class AddEscape {

    static unescapeSingleQuotedString(value) {
        return value.replace(/\'\'/g, '\'');
    }

    /**
     Unescapes a double quoted string.
     @return [String]      The unescaped string.
     * @param value
     */
    static unescapeDoubleQuotedString(value) {
        if (this._unescapeCallback == null) {
            this._unescapeCallback = (str) => {
                return this.unescapeCharacter(str);
            };
        }
        // Evaluate the string
        return REG.REG_ESCAPED_CHARACTER.replace(value, this._unescapeCallback);
    }

    /**
     Unescapes a character that was found in a double-quoted string
     @return [String]      The unescaped character
     * @param value
     */
    static unescapeCharacter(value) {
        switch (value.charAt(1)) {
            case '0':
                return fromCharCode(0);
            case 'a':
                return fromCharCode(7);
            case 'b':
                return fromCharCode(8);
            case 't':
                return '\t';
            case '\t':
                return '\t';
            case 'n':
                return '\n';
            case 'v':
                return fromCharCode(11);
            case 'f':
                return fromCharCode(12);
            case 'r':
                return fromCharCode(13);
            case 'e':
                return fromCharCode(27);
            case ' ':
                return ' ';
            case '"':
                return '"';
            case '/':
                return '/';
            case '\\':
                return '\\';
            case 'N':
                // U+0085 NEXT LINE
                return fromCharCode(0x0085);
            case '_':
                // U+00A0 NO-BREAK SPACE
                return fromCharCode(0x00A0);
            case 'L':
                // U+2028 LINE SEPARATOR
                return fromCharCode(0x2028);
            case 'P':
                // U+2029 PARAGRAPH SEPARATOR
                return fromCharCode(0x2029);
            case 'x':
                return Utils.utf8chr(Utils.hexDec(value.substr(2, 2)));
            case 'u':
                return Utils.utf8chr(Utils.hexDec(value.substr(2, 4)));
            case 'U':
                return Utils.utf8chr(Utils.hexDec(value.substr(2, 8)));
            default:
                return '';
        }
    }

}

module.exports = {AddEscape: AddEscape};
