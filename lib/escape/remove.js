const {escape, REG} = require('../constants.js');

/*
    Class RemoveEscape encapsulates escaping rules for single
    and double-quoted YML strings.
*/
class RemoveEscape {
    static MAPPING_ESCAPEES_TO_ESCAPED = () => {
        let i, j, mapping, ref;
        mapping = {};
        for (i = j = 0, ref = escape.LIST_ESCAPEES.length; (0 <= ref ? j < ref : j > ref); i = 0 <= ref ? ++j : --j) {
            mapping[escape.LIST_ESCAPEES[i]] = escape.LIST_ESCAPED[i];
        }
        return mapping;
    };
    /**
     Determines if a JavaScript value would require double quoting in YML.
     @return [Boolean] true    if the value would require double quotes.
     * @param value
     */
    static requiresDoubleQuoting(value) {
        return REG.REG_CHARACTERS_TO_ESCAPE.test(value);
    }

    /**
     Escapes and surrounds a JavaScript value with double quotes.
     @return [String]  The quoted, escaped string
     * @param value
     */
    static escapeWithDoubleQuotes(value) {
        let result;
        result = REG.REG_MAPPING_ESCAPEES.replace(value, (str) => {
            return this.MAPPING_ESCAPEES_TO_ESCAPED[str];
        });
        return '"' + result + '"';
    }

    /**
     Determines if a JavaScript value would require single quoting in YML.
     @return [Boolean] true if the value would require single quotes.
     * @param value
     */
    static requiresSingleQuoting(value) {
        return REG.REG_SINGLE_QUOTING.test(value);
    }

    /**
     Escapes and surrounds a JavaScript value with single quotes.
     @return [String]  The quoted, escaped string
     * @param value
     */
    static escapeWithSingleQuotes(value) {
        return "'" + value.replace(/'/g, "''") + "'";
    }

}

module.exports = {RemoveEscape};
