const {Parser} = require('./Parser'),
    {Utils} = require('./lib/utils');

/**
 * Class YamlReader offers convenience methods to read and convert YML to JSON.
 */
class YamlReader {
    /**
         Parses YML into a JavaScript object.
         The parse method, when supplied with a YML string,
         will do its best to convert YML in a file into a JavaScript object.
         @example
         myObject = YamlReader.parse('some: yaml');
         console.log(myObject);
         @return [Object]  The YML converted to a JavaScript object
         * @param input
         * @param exceptionOnInvalidType
         * @param objectDecoder
         @throw [ParseError] If the YML is not valid
     */
    static parse(input, exceptionOnInvalidType = false, objectDecoder = null) {
        return new Parser().parse(input, exceptionOnInvalidType, objectDecoder);
    }
    /**
     Read yml file from path given
     @example
     const file = path.resolve(__dirname, filePath);
     const ymlStr = YamlReader.read(file);
     console.log(ymlStr);
     @return [Object]  The YML converted to a JavaScript object
     * @param path
     * @param callback
     * @param exceptionOnInvalidType
     * @param objectDecoder
     @throw [ParseError] If the YML is not valid
     */
    static read(path, callback, exceptionOnInvalidType, objectDecoder) {
        return this.parseFile(path, callback, exceptionOnInvalidType, objectDecoder);
    }

    /**
     @return [Object]  The YML converted to a JavaScript object or null if the file doesn't exist.
     @throw [ParseError] If the YML is not valid
     */
    static parseFile(path, callback = null, exceptionOnInvalidType = false, objectDecoder = null) {
        let input;
        if (callback != null) {
            return Utils.getStringFromFile(path, (input) => {
                let result;
                result = null;
                if (input != null) {
                    result = this.parse(input, exceptionOnInvalidType, objectDecoder);
                }
                callback(result);
            });
        } else {
            input = Utils.getStringFromFile(path);
            if (input != null) {
                return this.parse(input, exceptionOnInvalidType, objectDecoder);
            }
            return null;
        }
    }
}

module.exports = {YamlReader};
