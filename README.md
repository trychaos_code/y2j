## Classes

<dl>
<dt><a href="#YamlReader">YamlReader</a></dt>
<dd><p>Class YamlReader offers convenience methods to read and convert YML to JSON.</p>
</dd>
<dt><a href="#LineAnalyzer">LineAnalyzer</a></dt>
<dd><p>LineAnalyzer YML parsing</p>
</dd>
<dt><a href="#Utils">Utils</a></dt>
<dd><p>Class A bunch of utility methods</p>
</dd>
<dt><a href="#Parser">Parser</a></dt>
<dd><p>Class Parser parses YML strings to convert them to JavaScript objects.</p>
</dd>
</dl>

<a name="YamlReader"></a>

## YamlReader
Class YamlReader offers convenience methods to read and convert YML to JSON.

**Kind**: global class  

* [YamlReader](#YamlReader)
    * [.parse(input, exceptionOnInvalidType, objectDecoder)](#YamlReader.parse) ⇒
    * [.read(path, callback, exceptionOnInvalidType, objectDecoder)](#YamlReader.read) ⇒
    * [.parseFile()](#YamlReader.parseFile) ⇒

<a name="YamlReader.parse"></a>

### YamlReader.parse(input, exceptionOnInvalidType, objectDecoder) ⇒
Parses YML into a JavaScript object.
         The parse method, when supplied with a YML string,
         will do its best to convert YML in a file into a JavaScript object.

**Kind**: static method of [<code>YamlReader</code>](#YamlReader)  
**Returns**: [Object]  The YML converted to a JavaScript object  
**Throw**: [ParseError] If the YML is not valid  

| Param | Default |
| --- | --- |
| input |  | 
| exceptionOnInvalidType | <code>false</code> | 
| objectDecoder | <code></code> | 

**Example**  
```js
myObject = YamlReader.parse('some: yaml');
         console.log(myObject);
         
```
<a name="YamlReader.read"></a>

### YamlReader.read(path, callback, exceptionOnInvalidType, objectDecoder) ⇒
Read yml file from path given

**Kind**: static method of [<code>YamlReader</code>](#YamlReader)  
**Returns**: [Object]  The YML converted to a JavaScript object  
**Throw**: [ParseError] If the YML is not valid  

| Param |
| --- |
| path | 
| callback | 
| exceptionOnInvalidType | 
| objectDecoder | 

**Example**  
```js
const file = path.resolve(__dirname, filePath);
     const ymlStr = YamlReader.read(file);
     console.log(ymlStr);
     
```
<a name="YamlReader.parseFile"></a>

### YamlReader.parseFile() ⇒
**Kind**: static method of [<code>YamlReader</code>](#YamlReader)  
**Returns**: [Object]  The YML converted to a JavaScript object or null if the file doesn't exist.  
**Throw**: [ParseError] If the YML is not valid  
<a name="LineAnalyzer"></a>

## LineAnalyzer
LineAnalyzer YML parsing

**Kind**: global class  

* [LineAnalyzer](#LineAnalyzer)
    * [.configure(exceptionOnInvalidType, objectDecoder)](#LineAnalyzer.configure)
    * [.parse(value, exceptionOnInvalidType, objectDecoder)](#LineAnalyzer.parse) ⇒
    * [.parseScalar(scalar, delimiters, stringDelimiters, context, evaluate)](#LineAnalyzer.parseScalar) ⇒
    * [.parseQuotedScalar(scalar, context)](#LineAnalyzer.parseQuotedScalar) ⇒
    * [.parseSequence(sequence, context)](#LineAnalyzer.parseSequence) ⇒
    * [.parseMapping(mapping, context)](#LineAnalyzer.parseMapping) ⇒
    * [.evaluateScalar(scalar, context)](#LineAnalyzer.evaluateScalar) ⇒

<a name="LineAnalyzer.configure"></a>

### LineAnalyzer.configure(exceptionOnInvalidType, objectDecoder)
**Kind**: static method of [<code>LineAnalyzer</code>](#LineAnalyzer)  

| Param | Default |
| --- | --- |
| exceptionOnInvalidType | <code></code> | 
| objectDecoder | <code></code> | 

<a name="LineAnalyzer.parse"></a>

### LineAnalyzer.parse(value, exceptionOnInvalidType, objectDecoder) ⇒
Converts a YML string to a JavaScript object.

**Kind**: static method of [<code>LineAnalyzer</code>](#LineAnalyzer)  
**Returns**: [Object]  A JavaScript object representing the YML string  
**Throw**: [ParseError]  

| Param | Default |
| --- | --- |
| value |  | 
| exceptionOnInvalidType | <code>false</code> | 
| objectDecoder | <code></code> | 

<a name="LineAnalyzer.parseScalar"></a>

### LineAnalyzer.parseScalar(scalar, delimiters, stringDelimiters, context, evaluate) ⇒
Parses a scalar to a YML string.

**Kind**: static method of [<code>LineAnalyzer</code>](#LineAnalyzer)  
**Returns**: [String]  A YML string  
**Throw**: [ParseError] When malformed inline YML string is parsed  

| Param | Default |
| --- | --- |
| scalar |  | 
| delimiters | <code></code> | 
| stringDelimiters |  | 
| context | <code></code> | 
| evaluate | <code>true</code> | 

<a name="LineAnalyzer.parseQuotedScalar"></a>

### LineAnalyzer.parseQuotedScalar(scalar, context) ⇒
Parses a quoted scalar to YML.

**Kind**: static method of [<code>LineAnalyzer</code>](#LineAnalyzer)  
**Returns**: [String]  A YML string  
**Throw**: [ParseError] When malformed inline YML string is parsed  

| Param |
| --- |
| scalar | 
| context | 

<a name="LineAnalyzer.parseSequence"></a>

### LineAnalyzer.parseSequence(sequence, context) ⇒
Parses a sequence to a YML string.

**Kind**: static method of [<code>LineAnalyzer</code>](#LineAnalyzer)  
**Returns**: [String]  A YML string  
**Throw**: [ParseError] When malformed inline YML string is parsed  

| Param |
| --- |
| sequence | 
| context | 

<a name="LineAnalyzer.parseMapping"></a>

### LineAnalyzer.parseMapping(mapping, context) ⇒
Parses a mapping to a YML string.

**Kind**: static method of [<code>LineAnalyzer</code>](#LineAnalyzer)  
**Returns**: [String]  A YML string  
**Throw**: [ParseError] When malformed inline YML string is parsed  

| Param |
| --- |
| mapping | 
| context | 

<a name="LineAnalyzer.evaluateScalar"></a>

### LineAnalyzer.evaluateScalar(scalar, context) ⇒
Evaluates scalars and replaces magic values.

**Kind**: static method of [<code>LineAnalyzer</code>](#LineAnalyzer)  
**Returns**: [String]  A YML string  

| Param |
| --- |
| scalar | 
| context | 

<a name="Utils"></a>

## Utils
Class A bunch of utility methods

**Kind**: global class  

* [Utils](#Utils)
    * [.trim(str, _char)](#Utils.trim) ⇒
    * [.ltrim(str, _char)](#Utils.ltrim) ⇒
    * [.isEmpty(value)](#Utils.isEmpty) ⇒
    * [.getStringFromFile(path, callback)](#Utils.getStringFromFile) ⇒
    * [.getFiles(directoryPath)](#Utils.getFiles) ⇒
    * [.stringToDate(str)](#Utils.stringToDate) ⇒
    * [.isNumeric(input)](#Utils.isNumeric) ⇒
    * [.isEmptyObject(value)](#Utils.isEmptyObject) ⇒
    * [.isDigits(input)](#Utils.isDigits) ⇒
    * [.octDec(input)](#Utils.octDec) ⇒
    * [.subStrCount(string, subString, start, length)](#Utils.subStrCount) ⇒
    * [.hexDec(input)](#Utils.hexDec) ⇒
    * [.utf8chr(c)](#Utils.utf8chr) ⇒

<a name="Utils.trim"></a>

### Utils.trim(str, _char) ⇒
Precompiled date pattern
     Trims the given string on both sides

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [String] A trimmed string  

| Param | Default |
| --- | --- |
| str |  | 
| _char | <code>\s</code> | 

<a name="Utils.ltrim"></a>

### Utils.ltrim(str, _char) ⇒
Trims the given string on the left side

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [String] A trimmed string  

| Param | Default |
| --- | --- |
| str |  | 
| _char | <code>\s</code> | 

<a name="Utils.isEmpty"></a>

### Utils.isEmpty(value) ⇒
Checks if the given value is empty (null, undefined, empty string, string '0', empty Array, empty Object)

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Boolean] true if the value is empty  

| Param |
| --- |
| value | 

<a name="Utils.getStringFromFile"></a>

### Utils.getStringFromFile(path, callback) ⇒
Reads the data from the given file path and returns the result as string

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [String]  The resulting data as string  

| Param | Default |
| --- | --- |
| path |  | 
| callback | <code></code> | 

<a name="Utils.getFiles"></a>

### Utils.getFiles(directoryPath) ⇒
listing all files in directoryPath

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Array] Array of files  

| Param |
| --- |
| directoryPath | 

<a name="Utils.stringToDate"></a>

### Utils.stringToDate(str) ⇒
Returns a parsed date from the given string

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Date] The parsed date or null if parsing failed  

| Param |
| --- |
| str | 

<a name="Utils.isNumeric"></a>

### Utils.isNumeric(input) ⇒
Returns true if input is numeric

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Boolean] true if input is numeric  

| Param |
| --- |
| input | 

<a name="Utils.isEmptyObject"></a>

### Utils.isEmptyObject(value) ⇒
Checks if the given value is an empty object

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Boolean] true if the value is empty and is an object  

| Param |
| --- |
| value | 

<a name="Utils.isDigits"></a>

### Utils.isDigits(input) ⇒
Returns true if input is only composed of digits

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Boolean] true if input is only composed of digits  

| Param |
| --- |
| input | 

<a name="Utils.octDec"></a>

### Utils.octDec(input) ⇒
Decode octal value

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Integer] The decoded value  

| Param |
| --- |
| input | 

<a name="Utils.subStrCount"></a>

### Utils.subStrCount(string, subString, start, length) ⇒
Counts the number of occurences of subString inside string

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Integer] The number of occurences  

| Param |
| --- |
| string | 
| subString | 
| start | 
| length | 

<a name="Utils.hexDec"></a>

### Utils.hexDec(input) ⇒
Decode hexadecimal value

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [Integer] The decoded value  

| Param |
| --- |
| input | 

<a name="Utils.utf8chr"></a>

### Utils.utf8chr(c) ⇒
Get the UTF-8 character for the given code point.

**Kind**: static method of [<code>Utils</code>](#Utils)  
**Returns**: [String] The corresponding UTF-8 character  

| Param |
| --- |
| c | 

<a name="Parser"></a>

## Parser
Class Parser parses YML strings to convert them to JavaScript objects.

**Kind**: global class  

* [Parser](#Parser)
    * [.cleanup(value)](#Parser+cleanup) ⇒
    * [.getRealCurrentLineNb()](#Parser+getRealCurrentLineNb) ⇒
    * [.isNextLineIndented()](#Parser+isNextLineIndented) ⇒
    * [.getCurrentLineIndentation()](#Parser+getCurrentLineIndentation) ⇒
    * [.getNextEmbedBlock(indentation, includeUnindentedCollection)](#Parser+getNextEmbedBlock) ⇒
    * [.isStringUnIndentedCollectionItem()](#Parser+isStringUnIndentedCollectionItem) ⇒
    * [.isCurrentLineComment()](#Parser+isCurrentLineComment) ⇒
    * [.isCurrentLineBlank()](#Parser+isCurrentLineBlank) ⇒
    * [.parseValue(value, exceptionOnInvalidType, objectDecoder)](#Parser+parseValue) ⇒
    * [.isNextLineUnIndentedCollection()](#Parser+isNextLineUnIndentedCollection) ⇒

<a name="Parser+cleanup"></a>

### parser.cleanup(value) ⇒
Cleanups a YML string to be parsed.

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [String]  A cleaned up YML string  

| Param |
| --- |
| value | 

<a name="Parser+getRealCurrentLineNb"></a>

### parser.getRealCurrentLineNb() ⇒
Returns the current line number (takes the offset into account).

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [Integer]     The current line number  
<a name="Parser+isNextLineIndented"></a>

### parser.isNextLineIndented() ⇒
Returns true if the next line is indented.

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [Boolean]     Returns true if the next line is indented, false otherwise  
<a name="Parser+getCurrentLineIndentation"></a>

### parser.getCurrentLineIndentation() ⇒
Returns the current line indentation.

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [Integer]     The current line indentation  
<a name="Parser+getNextEmbedBlock"></a>

### parser.getNextEmbedBlock(indentation, includeUnindentedCollection) ⇒
Returns the next embed block of YML.

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [String]          A YML string  
**Throw**: [ParseError]   When indentation problem are detected  

| Param | Default |
| --- | --- |
| indentation | <code></code> | 
| includeUnindentedCollection | <code>false</code> | 

<a name="Parser+isStringUnIndentedCollectionItem"></a>

### parser.isStringUnIndentedCollectionItem() ⇒
Returns true if the string is un-indented collection item

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [Boolean]     Returns true if the string is un-indented collection item, false otherwise  
<a name="Parser+isCurrentLineComment"></a>

### parser.isCurrentLineComment() ⇒
Returns true if the current line is a comment line.

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [Boolean]     Returns true if the current line is a comment line, false otherwise  
<a name="Parser+isCurrentLineBlank"></a>

### parser.isCurrentLineBlank() ⇒
Returns true if the current line is blank.

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [Boolean]     Returns true if the current line is blank, false otherwise  
<a name="Parser+parseValue"></a>

### parser.parseValue(value, exceptionOnInvalidType, objectDecoder) ⇒
Parses a YML value.

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [Object] A JavaScript value  
**Throw**: [ParseError] When reference does not exist  

| Param |
| --- |
| value | 
| exceptionOnInvalidType | 
| objectDecoder | 

<a name="Parser+isNextLineUnIndentedCollection"></a>

### parser.isNextLineUnIndentedCollection() ⇒
Returns true if the next line starts unindented collection

**Kind**: instance method of [<code>Parser</code>](#Parser)  
**Returns**: [Boolean]     Returns true if the next line starts unindented collection, false otherwise  
